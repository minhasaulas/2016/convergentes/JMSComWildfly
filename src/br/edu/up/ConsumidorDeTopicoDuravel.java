package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConsumidorDeTopicoDuravel {

  public static void main(String[] args) throws NamingException, JMSException {

    // 1. Cria��o do contexto, do t�pico e obten��o do factory;
    Context ctx = new InitialContext();
    Topic topico = (Topic) ctx.lookup("jms/TOPICO_DE_EXEMPLO");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 2. Cria��o da conex�o, defini��o do ID do cliente e start;
    Connection con = cf.createConnection("up", "positivo");
    con.setClientID("cliente");
    con.start();

    // 3. Cria��o da sess�o e do consumidor dur�vel;
    Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer consumidor = session.createDurableConsumer(topico, "pedidos");

    // 4. Leitura das TextMessages;
    System.out.println("Aguardando mensagem...");
    TextMessage msg = (TextMessage) consumidor.receive();
    System.out.println(msg.getText());

    // 5. Encerramento da conex�o e do contexto;
    con.close();
    ctx.close();
  }
}